import "./App.css";
import { PostProvider } from "./contextApi/postContext";
import Posts from "./posts";

function App() {
  return (
    <PostProvider>
      <Posts />
    </PostProvider>
  );
}

export default App;
