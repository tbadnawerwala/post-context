import React, {
  useState,
  createContext,
  useReducer,
  useContext,
  useEffect,
} from "react";
import axios from "axios";

export const postContext = createContext();
export const usePostcontext = () => useContext(postContext); //creating our own useContext hook

export const PostProvider = ({ children }) => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const { data } = await axios.get(
        `https://jsonplaceholder.typicode.com/posts`
      );
      setPosts(data);
    }
    fetchData();
  }, []);

  return (
    <postContext.Provider value={{ posts }}>{children}</postContext.Provider>
  );
};
