import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { usePostcontext } from "./contextApi/postContext";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    maxWidth: 275,
    height: 280,
    display: "inline-block",
    margin: "2%",
  },

  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  titleData: {
    fontSize: "1rem",
  },
  pos: {
    marginBottom: 12,
  },
});
const Posts = () => {
  const { posts } = usePostcontext();
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  return posts.map((item) => {
    return (
      <Card className={classes.root}>
        <CardContent>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Title:
          </Typography>
          <Typography className={classes.titleData} variant="h5" component="h2">
            {item.title}
            <br />
          </Typography>
          <br />
          <Typography>
            Description: <br />
          </Typography>
          <Typography variant="body2" component="p">
            {item.body}
            <br />
          </Typography>
        </CardContent>
        <CardActions></CardActions>
      </Card>
    );
  });
};

export default Posts;
